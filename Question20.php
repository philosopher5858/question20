

<!DOCTYPE html>
<html lang="en">
<head>
   <style>
 .header{
   position:absolute;
   width: 480px;
   height: 550px;
   border-radius: 10px;
   padding: 40px;
   font-size: 17px;
   box-sizing: border-box;
   /* background:#ecf0f3; */
   box-shadow: 14px 14px 20px #cbced1, -14px -14px 20px white;
   letter-spacing: 2px;
   border-right-style:inset ;
   border-right-color: red ;
   border-left-style: outset;
   border-left-color: red;
   

 }
 
 body {
   margin: 0;
   width: 100vw;
   height: 100vh;
   background: #ecf0f3;
   display: flex;
   align-items: center;
   text-align: center;
   justify-content: center;
   place-items: center;
   overflow: hidden;
   font-family: 'Courier New', Courier, monospace;
 }
 
 .container {
   position: relative;
   width: 500px;
   height: 100px;
   border-radius: 20px;
   padding: 40px;
   border-right-style:inset ;
   border-left-style:outset ;
   border-right-color:greenyellow ;
   border-left-color:greenyellow ;
   box-shadow: 14px 14px 20px #cbced1, -14px -14px 20px white;
   letter-spacing: 4px;
   
 }
 
 .inputs {
   text-align: left;
   margin-top: 3px;
 }
 
 label, input, button {
   display:inline;
   width: 80%;
   padding: 0;
   border: none;
   outline: none;
   box-sizing: border-box;
 }
 .lable2{
   margin-bottom: 40px;
   padding-right: 100%;
   font-family: 'Courier New', Courier, monospace;
 }
 
 label {
   margin-bottom: 40px;
   padding-right: 100%;
   font-family: 'Courier New', Courier, monospace;
 }
 
 label:nth-of-type(2) {
   margin-top: 12px;
   padding-right: 0%;
 }
 
 input::placeholder {
   color: gray;
 }
 
 input {
   background: #f7f9fb;
   padding: 10px;
   padding-left: 20px;
   height: 50px;
   font-size: 14px;
   border-radius: 14px;
   box-shadow: inset 2px 2px 2px rgb(191, 226, 139), inset -2px -2px 2px rgb(254, 210, 195);
 }
 
 button {
   color: white;
   margin-top: 20px;
   background: greenyellow;
   height: 40px;
   width: 220px;
   border-radius: 5px;
   cursor: pointer;
   font-weight: 900;
   box-shadow: 6px 6px 6px #cbced1, -6px -6px 6px white;
   transition: 0.5s;
  
   
 }
 
 button:hover {
   box-shadow: none; 
   background-color: red;
 }
 
 a {
   position: absolute;
   font-size: 8px;
   bottom: 4px;
   right: 4px;
   text-decoration: none;
   color: black;
   background: yellow;
   border-radius: 5px;
   padding: 2px;
 }
 
 h1 {
   position: absolute;
   top: 0;
   left: 0;
 }
 
   </style>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Document</title>
</head>
<body>
    <div class="header">
        Question 20. <br>WAP to check whether a given year is leap or not.
   </div>
   <form action="Question20.html" method="POST">
   
       <DIv class="container">
                       <?php
                       if($_SERVER['REQUEST_METHOD']=='POST'){
                      
                        $year = $_POST['year'];
                        print "YEAR: ".$year."<br><br>";

                        print "OUTPUT: <br>";
                      if(($year%4==0 && $year%100!=0)||($year%400==0)){
                        print "The Year Entered is Leap Year"."<br>";
                      }else{
                        print "Is Not Leap Year"."<br>";
                      }
                       }

                       ?>
              
                       <button type="submit">Check</button>
                
       </DIv>
   </form>
</body>
</html>